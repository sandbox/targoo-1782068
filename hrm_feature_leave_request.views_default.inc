<?php
/**
 * @file
 * hrm_feature_leave_request.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function hrm_feature_leave_request_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'hrm_leave_request_view';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'hrm_leave_request_view';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'hrm_leave_request_view';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Content: Leave request date */
  $handler->display->display_options['fields']['field_hrm_leave_request_date']['id'] = 'field_hrm_leave_request_date';
  $handler->display->display_options['fields']['field_hrm_leave_request_date']['table'] = 'field_data_field_hrm_leave_request_date';
  $handler->display->display_options['fields']['field_hrm_leave_request_date']['field'] = 'field_hrm_leave_request_date';
  $handler->display->display_options['fields']['field_hrm_leave_request_date']['label'] = '';
  $handler->display->display_options['fields']['field_hrm_leave_request_date']['element_type'] = '0';
  $handler->display->display_options['fields']['field_hrm_leave_request_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_hrm_leave_request_date']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_hrm_leave_request_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_hrm_leave_request_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  $handler->display->display_options['fields']['field_hrm_leave_request_date']['field_api_classes'] = TRUE;
  /* Field: Content: Leave request type */
  $handler->display->display_options['fields']['field_hrm_leave_request_type']['id'] = 'field_hrm_leave_request_type';
  $handler->display->display_options['fields']['field_hrm_leave_request_type']['table'] = 'field_data_field_hrm_leave_request_type';
  $handler->display->display_options['fields']['field_hrm_leave_request_type']['field'] = 'field_hrm_leave_request_type';
  $handler->display->display_options['fields']['field_hrm_leave_request_type']['label'] = '';
  $handler->display->display_options['fields']['field_hrm_leave_request_type']['element_type'] = '0';
  $handler->display->display_options['fields']['field_hrm_leave_request_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_hrm_leave_request_type']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_hrm_leave_request_type']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_hrm_leave_request_type']['field_api_classes'] = TRUE;
  /* Field: Content: Leave status */
  $handler->display->display_options['fields']['field_hrm_leave_status']['id'] = 'field_hrm_leave_status';
  $handler->display->display_options['fields']['field_hrm_leave_status']['table'] = 'field_data_field_hrm_leave_status';
  $handler->display->display_options['fields']['field_hrm_leave_status']['field'] = 'field_hrm_leave_status';
  $handler->display->display_options['fields']['field_hrm_leave_status']['label'] = '';
  $handler->display->display_options['fields']['field_hrm_leave_status']['element_type'] = '0';
  $handler->display->display_options['fields']['field_hrm_leave_status']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_hrm_leave_status']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_hrm_leave_status']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_hrm_leave_status']['field_api_classes'] = TRUE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'hrm_leave_request' => 'hrm_leave_request',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'hrm-leave-request-view';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Leave Request';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $export['hrm_leave_request_view'] = $view;

  return $export;
}
