<?php
/**
 * @file
 * hrm_feature_leave_request.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hrm_feature_leave_request_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function hrm_feature_leave_request_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function hrm_feature_leave_request_node_info() {
  $items = array(
    'hrm_leave_request' => array(
      'name' => t('Leave request'),
      'base' => 'node_content',
      'description' => t('HRM Leave request'),
      'has_title' => '1',
      'title_label' => t('Description'),
      'help' => '',
    ),
  );
  return $items;
}
