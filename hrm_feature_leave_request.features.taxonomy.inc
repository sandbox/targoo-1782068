<?php
/**
 * @file
 * hrm_feature_leave_request.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function hrm_feature_leave_request_taxonomy_default_vocabularies() {
  return array(
    'hrm_leave_type' => array(
      'name' => 'Leave Type',
      'machine_name' => 'hrm_leave_type',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
