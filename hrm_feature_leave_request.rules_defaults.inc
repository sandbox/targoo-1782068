<?php
/**
 * @file
 * hrm_feature_leave_request.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function hrm_feature_leave_request_default_rules_configuration() {
  $items = array();
  $items['rules_hrm_rules_after_saving_new_leave_request'] = entity_import('rules_config', '{ "rules_hrm_rules_after_saving_new_leave_request" : {
      "LABEL" : "hrm_rules_after_saving_new_leave_request",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HRM" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "hrm_leave_request" : "hrm_leave_request" } }
          }
        }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "A mail has been send to your manager for approval." } }
      ]
    }
  }');
  return $items;
}
